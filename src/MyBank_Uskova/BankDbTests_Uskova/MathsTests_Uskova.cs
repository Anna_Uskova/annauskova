﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using MyBank_Uskova;

namespace BankDbTests_Uskova
{
    [TestClass]
    public class MathsTests_Uskova
    {

        public TestContext TestContext { get; set; }

        [TestMethod]

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"D:\Uskova\Учеба\TRPO\Laba_3\MyBank_Uskova\BankDbTests_Uskova\data_Uskova.csv", "data_Uskova#csv", DataAccessMethod.Sequential)]
        public void AddIntegers_FromDataSourceTest()
        {
            var target = new Maths_Uskova();

            // Access the data
            int x = Convert.ToInt32(TestContext.DataRow["FirstNumber"]);
            int y = Convert.ToInt32(TestContext.DataRow["SecondNumber"]);
            int expected = Convert.ToInt32(TestContext.DataRow["Sum"]);
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }
    }
}
