﻿namespace CodedUITestProject1_Uskova
{
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;


    public partial class UIMap
    {

        public UIMap()
        {
            this.UIMainWindowWindow.UIStartButton.SearchProperties[WpfButton.PropertyNames.AutomationId] = "buttonA";
        }

        /// <summary>
        /// SimpleAppTest_Uskova1 - Используйте "SimpleAppTest_Uskova1Params" для передачи параметров в этот метод.
        /// </summary>
        public void ModifidedAppTest_Uskova1()
        {
            #region Variable Declarations
            WpfButton uIStartButton = this.UIMainWindowWindow.UIStartButton;
            WpfCheckBox uICheckBoxCheckBox = this.UIMainWindowWindow.UICheckBoxCheckBox;
            #endregion

            // Щелкните "Start" кнопка
            Mouse.Click(uIStartButton, new Point(50, 8));

            uICheckBoxCheckBox.WaitForControlEnabled();
            // Выбор "CheckBox" флажок
            uICheckBoxCheckBox.Checked = this.SimpleAppTest_Uskova1Params.UICheckBoxCheckBoxChecked;
        }

        public virtual SimpleAppTest_Uskova1Params SimpleAppTest_Uskova1Params
        {
            get
            {
                if ((this.mSimpleAppTest_Uskova1Params == null))
                {
                    this.mSimpleAppTest_Uskova1Params = new SimpleAppTest_Uskova1Params();
                }
                return this.mSimpleAppTest_Uskova1Params;
            }
        }

        private SimpleAppTest_Uskova1Params mSimpleAppTest_Uskova1Params;
    }
    /// <summary>
    /// Параметры для передачи в "SimpleAppTest_Uskova1"
    /// </summary>
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public class SimpleAppTest_Uskova1Params
    {

        #region Fields
        /// <summary>
        /// Выбор "CheckBox" флажок
        /// </summary>
        public bool UICheckBoxCheckBoxChecked = true;
        #endregion
    }
}
