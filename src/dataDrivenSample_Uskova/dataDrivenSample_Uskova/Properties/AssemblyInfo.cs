﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется с помощью следующего 
// набором атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные с этой сборкой.
[assembly: AssemblyTitle("dataDrivenSample_Uskova")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("dataDrivenSample_Uskova")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Установка параметра ComVisible в значение false делает типы в этой сборке невидимыми 
// для компонентов модели COM.  Если тип в этой сборке необходимо сделать доступным 
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("acd1a541-e4bc-4fbc-a75b-d4038b38223d")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номера сборки и редакции по умолчанию 
// используя "*", как показано ниже:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
